/****************************************************************************
* COSC2138/CPT 220 - Programming Principles 2A
* Study Period 2  2015 Assignment #1 - minesweeper program
* Full Name        : Paolo Felicelli
* Student Number   : 3427174
* Course Code      : CPT220
* Start up code provided by the CTeach Team
****************************************************************************/

/* Header files. */
#include <stdio.h>
#include <stdlib.h>

/* My header inclusions */
#include <string.h> /* string.h has been included 
                       to provide the strlen() function */
#include <ctype.h> /* ctype.h has been included 
                     to provide the isalpha() function */
#include <math.h> /* math.h has been included to
                     rand() function */
#include <time.h> /* time.h has been included to
                     provide the time() function */

/* Constants. */
#define MINE_DENSITY 0.16
#define MIN_GRID 2
#define MAX_GRID 16
#define FLAG 'F'
#define MINE 'M'
#define BAD_MINE 'X'
#define UNKNOWN '?'
#define BLANK ' '
#define FLAG_SQUARE 'f'
#define UNCOVER_SQUARE 'u'
#define SWEEP_SQUARE 's'

/* My constants */
#define MAX 2 
#define SINGLE_CHAR_INPUT 1
#define TEMP_STRING_LENGTH 1 
#define CELL_MAX 4 /* MAX/SINGLE_CHAR_INPUT/TEMP_STRING_LENGTH/CELL_MAX 
							 constants used for char array sizes */

#define INPUT_LENGTH 3
#define COL_MIN 1 /* Constants used for getColRow() when choosing a 
                     location on the grid */

#define TRUE 1  
#define FALSE 0 
#define SUCCESS 1
#define FAILURE 0
#define FINISHED 0 /* TRUE/FALSE/SUCCESS/FAILURE/FINISHED constants 
					 		 defined for general purpose boolean flags */

/* My macros */
#define NEW_LINE printf("\n") /* Prints a new line for formating text to 
											 screen */
#define ROUND_UP(x) x < 0 ? x - 0.5 : x + 0.5 /* ROUND_UP is a macro 
										that returns rounded up integer value */

/* Function prototypes. */
void init(char minefield[MAX_GRID][MAX_GRID], 
          char displayGrid[MAX_GRID][MAX_GRID]);
unsigned getSize();
void placeMines(char minefield[MAX_GRID][MAX_GRID], unsigned size);
void displayMinefield(char displayGrid[MAX_GRID][MAX_GRID], unsigned size);
char guessType();
void guessSquare(char* row, unsigned* col, unsigned size);
int processGuess(char minefield[MAX_GRID][MAX_GRID], 
                 char displayGrid[MAX_GRID][MAX_GRID],
                 unsigned size, char type, char row, unsigned col);
void readRestOfLine();

/* Borrowed prototypes */
int getInteger(unsigned* integer, unsigned length, 
					char* prompt, int min, int max);
int getString(char *string, int length, int minlength, char *prompt);

/* My prototypes */
int getChar(char *ch, unsigned length);
int *getRowCol(int *gridRef, unsigned length, int min, unsigned size);
int countMines(char minefield[MAX_GRID][MAX_GRID], int row, 
					int col, unsigned size);
int charToInt(char ch);
void floodFill(unsigned size, int row, int col, 
	char minefield[MAX_GRID][MAX_GRID], char displayGrid[MAX_GRID][MAX_GRID]); 
void processUncoverMine(char minefield[MAX_GRID][MAX_GRID], 
					    char displayGrid[MAX_GRID][MAX_GRID], 
					    unsigned size, char type, int row, int col);
void processUncoverNum(char minefield[MAX_GRID][MAX_GRID], 
                       char displayGrid[MAX_GRID][MAX_GRID], 
                       char type, int row, int col);
void flagSquare(char minefield[MAX_GRID][MAX_GRID], 
                   char displayGrid[MAX_GRID][MAX_GRID], 
                   int row, int col);
void sweepSquares(char minefield[MAX_GRID][MAX_GRID], 
                        char displayGrid[MAX_GRID][MAX_GRID],
                        int row, int col, unsigned size);





