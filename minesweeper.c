/****************************************************************************
* COSC2138/CPT 220 - Programming Principles 2A
* Study Period 2  2015 Assignment #1 - minesweeper program
* Full Name        : Paolo Felicelli
* Student Number   : 3427174
* Course Code      : CPT220
* Start up code provided by the CTeach Team
****************************************************************************/

#include "minesweeper.h"

/* Global variables hold a count of number of mines placed and number 
   of mines flagged */
unsigned mineQty; 
unsigned flagCnt;

/****************************************************************************
* Function main() is the entry point for the program.
****************************************************************************/
int main(void)
{
   char row, type;
   unsigned col;
   int *ptr;
   int gridRef[CELL_MAX];

   /* Stores all hidden data about the minefield. */
   char minefield[MAX_GRID][MAX_GRID];
   /* A version of the minefield that only stores known information. */
   char displayGrid[MAX_GRID][MAX_GRID];   
   /* Number of cells in each row and column used. */
   unsigned size = 0;

   /* Initialize minefields with UNKNOWN and BLANK characters */
   init(minefield, displayGrid);

   /* Call getSize to enter size of the minefield, use the returned value
      as parameter for various functions */
   size = getSize();

   /* Call placeMines() to place mines in the field */
   placeMines(minefield, size); 

   do
   {
      /* Display unknown minefield */
      displayMinefield(displayGrid, size);

      /* Call to guessType() to enter one of 3 options */ 
      type = guessType();  

      /* Get the grid coordinates off the user */
      ptr = getRowCol(gridRef, INPUT_LENGTH, COL_MIN, size);
      
      /* Dereference the returned values from getColRow() for pluging 
         in to guessSquare() and processGuess() */
      row = *(ptr);
      col = *(ptr + 1);

      /* If user selects column a1, that selection is actually [0][0] on the 
      2D grid, subtracting 1 from the visual representation of the grid
      reference gives the user the correct input for the underlying
      array reference */
      col = col - 1;

      /* Call the guessSquare() function with the separately extracted
          row and col components as parameters */
      guessSquare(&row, &col, size);
      
   } while(processGuess(minefield, displayGrid, size, type, row, col) 
                                                         == FINISHED);

   /* Display minefield */
   displayMinefield(displayGrid, size);

   return EXIT_SUCCESS;
}


/****************************************************************************
* Function init() initialises the "minefield" array with BLANK characters,
* and the "displayGrid" array with UNKNOWN characters. These constants (and
* others) are obtained from the header file.
****************************************************************************/
void init(char minefield[MAX_GRID][MAX_GRID], 
          char displayGrid[MAX_GRID][MAX_GRID])
{
   int i, j;

   /* Nested for loops initialize grids with UNKNOWN and BLANK chars. */
   for(i = 0; i < MAX_GRID; i++)  
   {
      for(j = 0; j < MAX_GRID; j++)
      {
         displayGrid[i][j] = UNKNOWN;
         minefield[i][j] = BLANK;
      }
   }

   NEW_LINE;
   NEW_LINE;
   printf(" Cells initialized.....\n\n");
}


/****************************************************************************
* Function getSize() prompts the user for the size of the minefield in the
* range MIN_GRID to MAX_GRID. Example:
* ------------------------------------------------------
* Enter minefield size (2-16): 6
* You have chosen a minefield with 6 rows and 6 columns.
* ------------------------------------------------------
****************************************************************************/
unsigned getSize() 
{
   char input[MAX];
   int isValid = FALSE;
   unsigned fieldSize;

   /* Loop mechanism that requests user for correct input */
   while(isValid == FALSE)
   {
      /* Read from a string and return it as an int */
      fieldSize = sscanf(input, "%u", &fieldSize);

      /* Call getInteger() to perform validation */
      getInteger(&fieldSize, MAX, " Enter minefield size (2-16): ", 
                 MIN_GRID, MAX_GRID);

      /* Flip isValid to true after correct input to break out of loop */
      isValid = TRUE; 
   }

   printf(" You have chosen a minefield with %d rows and %d columns.\n\n", 
      fieldSize, fieldSize);

   return fieldSize;
}


/****************************************************************************
* Function placeMines() places a number of mines in the minefield. The 
* number of mines placed is equal to the number of squares in the minefield
* multiplied by the MINE_DENSITY constant.
* For example, a grid with size 6 has 36 squares.
* 36 x 0.16 = 5.76 or approximately 6 mines. (Result is always rounded up.)
* The mines are placed at random positions and each mine must be in a 
* different square. Use the MINE constant to mark mine squares. Use numbers
* from 1-8 to mark other squares that have that number of mines adjacent to
* them. Squares that don't have adjacent mines are marked as BLANK.
* Here's an example:
* ---------------------------
*     1   2   3   4   5   6
*   +---+---+---+---+---+---+ [ EXAMPLE 1]
* a | M | M | 1 |   |   |   |
*   +---+---+---+---+---+---+
* b | 2 | 2 | 1 |   |   |   |
*   +---+---+---+---+---+---+
* c |   |   | 1 | 1 | 1 |   |
*   +---+---+---+---+---+---+
* d |   |   | 2 | M | 2 |   |
*   +---+---+---+---+---+---+
* e |   |   | 3 | M | 4 | 1 |
*   +---+---+---+---+---+---+
* f |   |   | 2 | M | M | 1 |
*   +---+---+---+---+---+---+
* ---------------------------
****************************************************************************/
void placeMines(char minefield[MAX_GRID][MAX_GRID], unsigned size)
{
   int numOfSquares, m, numOfMines, row, col, mineCnt;
   float roundUp;

   /* Calculate the number of squares in the grid */
   numOfSquares = size * size;

   /* Calculate the number of mines respective to the size of the grid */
   roundUp = numOfSquares * MINE_DENSITY;

   /* ROUND_UP is a macro that returns the rounded up integer value of 
      roundUp */
   numOfMines = ROUND_UP(roundUp);

   /* Seeding srand() with time(0) so that rand() does not produce the
      same numbers every time */
   srand(time(0));

   /* Loop mechanism to place mines at random locations */
   for(m = 0; m < numOfMines; m++)
   {
      row = rand() % size;
      col = rand() % size;
     
      if(minefield[row][col] != MINE)
      {
         minefield[row][col] = MINE; 
         /* Tally up the quantity of mines in the field */
         mineQty++; 
      }   
   }

   /* Loop mechanism that calls countMines(), returns a count of the 
      mines adjacent to each square */
   for(row = 0; row < size; row++)  
   {
      for(col = 0; col < size; col++)
      {
         mineCnt = countMines(minefield, row, col, size);

         if(mineCnt != 0 && minefield[row][col] != MINE)
         {
            /* Convert count to ascii equivilant and assign to 
               minefield[row][col] */
            minefield[row][col] = '0' + mineCnt;
         }
      }
   }

   /* Debug mechanism for viewing the values inside the minefield
      grid - not part of the actual game code 
   for(row = 0; row < size; row++)  
   {
      for(col = 0; col < size; col++)
      {
         printf("| %c ", minefield[row][col]);
      }
      printf("|\n");
   }*/
}


/****************************************************************************
* Function displayMinefield() shows the user an ascii-art drawing of the
* minefield with only known information displayed. Initially, nothing is
* known about the minefield, so all squares are hidden. Example:
* ---------------------------
*     1   2   3   4   5   6
*   +---+---+---+---+---+---+ [ EXAMPLE 2]
* a | ? | ? | ? | ? | ? | ? |
*   +---+---+---+---+---+---+
* b | ? | ? | ? | ? | ? | ? |
*   +---+---+---+---+---+---+
* c | ? | ? | ? | ? | ? | ? |
*   +---+---+---+---+---+---+
* d | ? | ? | ? | ? | ? | ? |
*   +---+---+---+---+---+---+
* e | ? | ? | ? | ? | ? | ? |
*   +---+---+---+---+---+---+
* f | ? | ? | ? | ? | ? | ? |
*   +---+---+---+---+---+---+
* You have flagged 0/6 mines.
* ---------------------------
****************************************************************************/
void displayMinefield(char displayGrid[MAX_GRID][MAX_GRID], unsigned size)
{
   int i, m, row, col;
   char c = 'a';
   
   printf("    ");

   /* Print the numbers 1 to size on top of the grid */
   for(i = 1; i <= size; i++)
   {
      printf("%2d  ", i);
   }

   printf("\n");

      for(row = 0; row < size; row++)
      {
         /* Print (a - size) and +--- characters for the grid */
         printf("   ");

         for(m = 0; m < size; m++)
         {
            printf("+---");
         }

            printf("+\n");
            printf(" %c ", c);

            /* Print grid values */
            for(col = 0; col < size; col++)
            {
               printf("| %c ", displayGrid[row][col]);
            }

            printf("|\n");
            c++;  
      }

      /* Print bottom +--- characters for the grid */
      printf("   ");

      for(i = 0; i < size; i++)
      {
         printf("+---");
      }

      printf("+\n\n");

      /* Dump formatted known flagged mines stats */
      printf(" You have flagged %u/%u mines.\n\n", flagCnt, mineQty);
}


/****************************************************************************
* Function guessType() prompts the user for a single character representing
* one of three options. These options are used in the processGuess() 
* function. Example:
* --------------------------------------------------
* Enter guess type ((f)lag, (u)ncover or (s)weep): u
* You have selected "uncover".
* --------------------------------------------------
****************************************************************************/
char guessType()
{  
   char ch[SINGLE_CHAR_INPUT + 1];

   getChar(ch, SINGLE_CHAR_INPUT);

   if(*ch == FLAG_SQUARE)
   {
      printf(" You have selected \"flag\"\n\n");
   }

   if(*ch == UNCOVER_SQUARE)
   {
      printf(" You have selected \"uncover\"\n\n");
   }

   if(*ch == SWEEP_SQUARE)
   {
      printf(" You have selected \"sweep\"\n\n");
   }

   return *ch;
}
   

/****************************************************************************
* Function guessSquare() prompts the user for the reference of a square 
* in the minefield. The row and column components are extracted and made
* available to the calling function separately. Example:
* ------------------------------------------
* Enter square (a1-f6): f1
* Your have selected row "f" and column "1".
* ------------------------------------------
****************************************************************************/
void guessSquare(char* row, unsigned* col, unsigned size)
{
   /* Add 1 to dereferenced col variable, for example, if user enters a1, 
   that is actually [0][0] on the grid, adding 1 to variable is just to show 
   the user that they have selected col 1 on the visiualized grid */
   printf(" Your have selected row '%c' and column '%d'\n\n", *row, *col + 1);
}


/****************************************************************************
* Function processGuess() manipulates the displayGrid variable and determines
* if the Minesweeper game is over or not. It processes the three types of
* actions called "(f)lag square", "(u)ncover square", and "(s)weep square".
* Examples:
*
* Consider the following minefield ("minefield" variable):
* ---------------------------
*     1   2   3   4   5   6
*   +---+---+---+---+---+---+ [ EXAMPLE 3]
* a | M | M | 1 |   |   |   |
*   +---+---+---+---+---+---+
* b | 2 | 2 | 1 |   |   |   |
*   +---+---+---+---+---+---+
* c |   |   | 1 | 1 | 1 |   |
*   +---+---+---+---+---+---+
* d |   |   | 2 | M | 2 |   |
*   +---+---+---+---+---+---+
* e |   |   | 3 | M | 4 | 1 |
*   +---+---+---+---+---+---+
* f |   |   | 2 | M | M | 1 |
*   +---+---+---+---+---+---+
* ---------------------------
* Initially, the minefield is entirely hidden ("displayGrid" variable):
* ---------------------------
*     1   2   3   4   5   6
*   +---+---+---+---+---+---+ [ EXAMPLE 4]
* a | ? | ? | ? | ? | ? | ? |
*   +---+---+---+---+---+---+
* b | ? | ? | ? | ? | ? | ? |                                                     
*   +---+---+---+---+---+---+
* c | ? | ? | ? | ? | ? | ? |
*   +---+---+---+---+---+---+
* d | ? | ? | ? | ? | ? | ? |
*   +---+---+---+---+---+---+
* e | ? | ? | ? | ? | ? | ? |
*   +---+---+---+---+---+---+
* f | ? | ? | ? | ? | ? | ? |
*   +---+---+---+---+---+---+
* ---------------------------
* If the user decides to "uncover square f1", the following can happen:
* - If a mine is accidentally uncovered, the game is lost.
* - If a numbered square is uncovered, this is marked in the "displayGrid"
*   variable.
* - If a blank square is uncovered, this is marked in the "displayGrid" 
*   variable and all adjacent blank squares above, below, left, or right
*   are also marked.
* In this example, square f1 is blank, so the "displayGrid" variable is
* updated to look like this:
* ---------------------------
*     1   2   3   4   5   6
*   +---+---+---+---+---+---+ [ EXAMPLE 5]
* a | ? | ? | ? | ? | ? | ? |
*   +---+---+---+---+---+---+
* b | 2 | 2 | 1 | ? | ? | ? |
*   +---+---+---+---+---+---+
* c |   |   | 1 | ? | ? | ? |
*   +---+---+---+---+---+---+
* d |   |   | 2 | ? | ? | ? |
*   +---+---+---+---+---+---+
* e |   |   | 3 | ? | ? | ? |
*   +---+---+---+---+---+---+
* f |   |   | 2 | ? | ? | ? |
*   +---+---+---+---+---+---+
* ---------------------------
* From the above information, the user can already deduce where five of the
* six mines are located. They are in squares a1, a2, d4, e4, and f4. So, the
* user would next want to "flag square a1" and the other four squares. 
* The user can only flag squares on unknown squares. So, our "displayGrid"
* variable now looks like this:
* ---------------------------
*     1   2   3   4   5   6
*   +---+---+---+---+---+---+ [ EXAMPLE 6]
* a | F | F | ? | ? | ? | ? |
*   +---+---+---+---+---+---+
* b | 2 | 2 | 1 | ? | ? | ? |
*   +---+---+---+---+---+---+
* c |   |   | 1 | ? | ? | ? |
*   +---+---+---+---+---+---+
* d |   |   | 2 | F | ? | ? |
*   +---+---+---+---+---+---+
* e |   |   | 3 | F | ? | ? |
*   +---+---+---+---+---+---+
* f |   |   | 2 | F | ? | ? |
*   +---+---+---+---+---+---+
* ---------------------------
* The user can now deduce that there are no mines in squares a3, a4, b4, and
* c4. The user can now uncover these squares one at a time if he or she
* wishes. However, this is somewhat inefficient. Alternatively, the user may
* want to "sweep" around a numbered square that is considered safe because 
* the adjacent mines have been (hopefully) correctly flagged.
* So the user can "sweep square b3". This effectively executes the "uncover"
* command on the surrounding 8 squares (but only the ones that are marked as
* UNKNOWN). After performing the "sweep square b3" command, the "displayGrid"
* variable looks like this:
* ---------------------------
*     1   2   3   4   5   6
*   +---+---+---+---+---+---+ [ EXAMPLE 7]
* a | F | F | 1 |   |   |   |
*   +---+---+---+---+---+---+
* b | 2 | 2 | 1 |   |   |   |
*   +---+---+---+---+---+---+
* c |   |   | 1 | 1 | 1 |   |
*   +---+---+---+---+---+---+
* d |   |   | 2 | F | 2 |   |
*   +---+---+---+---+---+---+
* e |   |   | 3 | F | 4 | 1 |
*   +---+---+---+---+---+---+
* f |   |   | 2 | F | ? | ? |
*   +---+---+---+---+---+---+
* ---------------------------
* The user will need to guess whether the final mine is in square f5 or f6
* and flag the square. When six squares are flagged, the user guesses are 
* verified. If any guess is incorrect, then the user loses and the game is
* over. Otherwise the user wins the game and the game is over. The 
* displayMinefield() function is called one last time with bad guesses marked
* as BAD_MINE.
*
* This function is a large one, and it is recommended that you further
* modularise it. Leave this one until you have implemented the other six
* major functions. Additionally, you should implement the three commands in
* this order: flag, uncover, sweep.
****************************************************************************/
int processGuess(char minefield[MAX_GRID][MAX_GRID], 
                 char displayGrid[MAX_GRID][MAX_GRID],
                 unsigned size, char type, char row, unsigned col)
{
   int intRow, i, j;

   /* Convert the row arg to an int */
   intRow = charToInt(row);

   /* Call to processUncoverMine(). Processes weather the user uncovers a 
      mine, if so then game is lost */
   if(type == UNCOVER_SQUARE && minefield[intRow][col] == MINE)
   {
      processUncoverMine(minefield, displayGrid, size, type, intRow, col);
      return !FINISHED;
   }

   /* Check if user uncovers a numbered square */
   if(type == UNCOVER_SQUARE && isdigit(minefield[intRow][col]))
   {
      processUncoverNum(minefield, displayGrid, type, intRow, col);
   }
   
   /* If user uncovers a blank square, call floodFill() to uncover 
      all squares adjacent to the guessed grid reference */
   if(type == UNCOVER_SQUARE)
   {
      floodFill(size, intRow, col, minefield, displayGrid);
   }
   
   /* Check if user flags a mine, and if so, tally up a count of flagged
      mines, check if user tries to flag a numbered square and check 
      if user tries to flag a blank square */
   if(type == FLAG_SQUARE)
   {
      flagSquare(minefield, displayGrid, intRow, col);
   }

   /* Call sweepSquares() to uncover the surrounding 8 squares, but 
      only if they are marked as UNKNOWN */
   if(type == SWEEP_SQUARE)
   {     
      sweepSquares(minefield, displayGrid, intRow, col, size);
   }

   /* Check count of flagged mines, if user flags all the mines then 
      user wins and game is over */
   if(flagCnt == mineQty)
   {
      for(i = 0; i < size; i++)
      {
         for(j = 0; j < size; j++)
         {
            /* Assign MINE to displayGrid for display when game is over */
            if(minefield[i][j] == MINE)
            {
               displayGrid[i][j] = MINE;
            }

            /* Assign BAD_MINE in location where user has incorrectly flagged 
               mines to displayGrid for display when game is over */
            if(minefield[i][j] != MINE && displayGrid[i][j] == FLAG)
            {
               displayGrid[i][j] = BAD_MINE;
            }
         }
      }

      printf(" Congratulations you win! All mines flagged!\n\n");

      return !FINISHED;
   }

   return FINISHED;
}


/****************************************************************************
* Function readRestOfLine() is used for buffer clearing. Source: 
* https://inside.cs.rmit.edu.au/~sdb/teaching/C-Prog/CourseDocuments/
* FrequentlyAskedQuestions/
****************************************************************************/
void readRestOfLine()
{
   int c;

   /* Read until the end of the line or end-of-file. */   
   while ((c = fgetc(stdin)) != '\n' && c != EOF)
      ;

   /* Clear the error and end-of-file flags. */
   clearerr(stdin);
}


/****************************************************************************
* getInteger(): An interactive integer input function using dynamic memory.
* This function prompts the user for an integer using a custom prompt. A line
* of text is accepted from the user using fgets() and stored in a temporary
* string. When the function detects that the user has entered too much text,
* an error message is given and the user is forced to reenter the input. The
* function also clears the extra text (if any) with the readRestOfLine()
* function.
* When a valid string has been accepted, the unnecessary newline character
* at the end of the string is overwritten. The function then attempts to 
* convert the string into an integer with strtol(). The function checks to
* see if the input is numberic and within range.
* Finally, the temporary integer is copied to the integer variable that is 
* returned to the calling function.
****************************************************************************
* Note: This code courtesy of RMIT Online Standard C Library Examples 
* - Sample Code. https://lms.rmit.edu.au/webapps/blackboard/content/
* listContent.jsp?course_id=_315940_1&content_id=_6094161_1
****************************************************************************/
int getInteger(unsigned* integer, unsigned length, 
               char* prompt, int min, int max)
{
   int finished = FALSE;
   char* tempString;
   int tempInteger = 0;
   char* endPtr;

   /* Allocate temporary memory. */
   if ((tempString = malloc(sizeof(char) * (length+2))) == NULL)
   {
      fprintf(stderr, "Fatal error: malloc() failed in getString().\n");
      exit(EXIT_FAILURE);
   }

   /* Continue to interact with the user until the input is valid. */
   do
   {
      /* Provide a custom prompt. */
      printf("%s", prompt);
      
      /* Accept input. "+2" is for the \n and \0 characters. */
      fgets(tempString, length + 2, stdin);

      /* A string that doesn't have a newline character is too long. */
      if (tempString[strlen(tempString) - 1] != '\n')
      {
         printf(" Input was too long.\n\n");
         readRestOfLine();
      }
      else
      {
         /* Overwrite the \n character with \0. */
         tempString[strlen(tempString) - 1] = '\0';

         /* Convert string to an integer. */
         tempInteger = (int) strtol(tempString, &endPtr, 10);

         /* Validate integer result. */
         if (strcmp(endPtr, "") != 0)
         {
            printf(" Input was not numeric.\n\n");
         }
         else if (tempInteger < min || tempInteger > max)
         {
            printf(" Input was not within range %d - %d.\n\n", min, max);
         }
         else
         {
            finished = TRUE;
         }
      }
   } while (finished == FALSE);

   /* Make the result string available to calling function. */
   *integer = tempInteger;

   /* Deallocate temporary memory. */
   free(tempString);

   return SUCCESS;
}


/****************************************************************************
* getChar() function prompts the user to enter a single character. Also 
* performs validation for input length and character matching. 
* Note: This function is a variation of the getInteger() function provided 
* by RMIT. It has been modified to be made of use for the guessType() 
* function.
****************************************************************************/
int getChar(char* ch, unsigned length)
{
   int isValid = FALSE;
   char tempString[TEMP_STRING_LENGTH + 2];

   /* Loop mechanism that requests user for correct input */
   while(isValid == FALSE)
   {
      /* Provide prompt */
      printf(" Enter guess type ((f)lag, (u)ncover or (s)weep): ");
      
      /* Accept input. "+2" is for the \n and \0 characters */
      fgets(tempString, length + 2, stdin);

      /* A string that doesn't have a newline character is too long. */
      if(tempString[strlen(tempString) - 1] != '\n')
      {
         printf(" Input was too long.\n");
         readRestOfLine();
      }

      /* Check that user enters correct character */
      if((*tempString != FLAG_SQUARE || *(tempString + 1) != '\n') 
         && (*tempString != UNCOVER_SQUARE || *(tempString + 1) != '\n') 
            && (*tempString != SWEEP_SQUARE || *(tempString + 1) != '\n')) 
      {
         printf(" Input must be f, u or s.\n\n");
      }

      else
      {
         /* Flip isValid to true after correct input to break out of loop */
         isValid = TRUE;
      }
   } 

   /* Overwrite the \n character with \0. */
   tempString[strlen(tempString) - 1] = '\0';
   
   /* Make the result string available to calling function. */
   strcpy(ch, tempString);

   return SUCCESS;
}


/*****************************************************************************
* getRowCol() function checks whether the user input is a valid character 
* between a - p and 1 - 16, it then returns an array where the values are 
* dereferenced for plugging in to their respective functions.
*****************************************************************************
* Note: This code is a modified version of the getInteger function provided 
* by RMIT Online Standard C Library Examples - Sample Code. 
* https://lms.rmit.edu.au/webapps/blackboard/content/listContent
* .jsp?course_id=_315940_1&content_id=_6094161_1
****************************************************************************/
int *getRowCol(int *gridRef, unsigned length, int min, unsigned size)
{
   int finished = FALSE;
   char *tempString;
   int tempInteger = 0;
   char ch;
   char c = 'a';

   /* Allocate temporary memory. */
   if((tempString = malloc(sizeof(char) * (length+2))) == NULL)
   {
      fprintf(stderr, "Fatal error: malloc() failed in getString().\n");
      exit(EXIT_FAILURE);
   }

   /* Continue to interact with the user until the input is valid. */
   do
   {
      /* Provide a custom prompt. */
      printf(" Enter a grid reference (a1 - %c%d): ", (c + size - 1), size);
      
      /* Accept input. "+2" is for the \n and \0 characters. */
      fgets(tempString, length + 2, stdin);

      /* A string that doesn't have a newline character is too long. */
      if(tempString[strlen(tempString) - 1] != '\n')
      {
         printf(" Input was too long.\n");
         readRestOfLine();
      }

      else
      {
         /* Overwrite the \n character with \0. */
         tempString[strlen(tempString) - 1] = '\0';

         ch = tempString[0];

         if(ch < 'a' || ch > (c + (size - 1)))
         {
            printf(" Invalid row input, must be lowercase (a - %c).\n", (c + size - 1));
            continue;
         }

         /* Store the first entry of tempString into gridRef array */
         gridRef[0] = ch;

         /* Convert tempInt to an integer. */
         tempInteger = atoi(tempString + 1);

         gridRef[1] = tempInteger;

         if (tempInteger < min || tempInteger > size)
         {
            printf(" Invalid column input, must be between  (%d - %d).\n", min, size);
         }

         else
         {
            finished = TRUE;
         }
      }

   }while (finished == FALSE);

   /* Deallocate temporary memory. */
   free(tempString);

   return gridRef;
}
         

/****************************************************************************
* getString(): An interactive string input function with dynamic memory.
* This function prompts the user for a string using a custom prompt. A line
* of text is accepted from the user using fgets() and stored in a temporary
* string. When the function detects that the user has entered too much text,
* an error message is given and the user is forced to reenter the input. The
* function also clears the extra text (if any) with the readRestOfLine()
* function.
* When a valid string has been accepted, the unnecessary newline character
* at the end of the string is overwritten. Finally, the temporary string is
* copied to the string variable that is returned to the calling function.
****************************************************************************
* Note: Portions modified from https://www.dlsweb.rmit.edu.au/set/Courses/
* Content/CSIT/oua/cpt220/c-function-examples/InputValidation/
* getString-advanced.c
****************************************************************************/
int getString(char *string, int length, int minlength, char *prompt)
{
   int finished = FALSE;
   char tempString[3];

   /* Continue to interact with the user until the input is valid. */
   do
   {
      /* Provide a custom prompt. */
      printf("%s", prompt);

      /* Accept input. "+2" is for the \n and \0 characters. */
      fgets(tempString, length + 2, stdin);

      /* A string that doesn't have a newline character is too long. */
      if (tempString[strlen(tempString) - 1] != '\n' )
      {
         printf(" Input was too long.....\n\n");
         readRestOfLine();
      }

      else if(strlen(tempString) < minlength + 1)
      {
         printf(" Input was too short.....\n\n");
      }

      else
      {
         finished = TRUE;
      }
   }
   while (finished == FALSE);

   /* Overwrite the \n character with \0. */
   tempString[strlen(tempString) - 1] = '\0';

   /* Make the result string available to calling function. */
   strcpy(string, tempString);

   return SUCCESS;
}


/****************************************************************************
* Function int roundNum(float num) rounds up a float to the nearest whole
* number. NOTE: no longer in use, using ROUND_UP macro instead for 
* educational purpose.
****************************************************************************/
/*int roundNum(float num)
{
   return num < 0 ? num - 0.5 : num + 0.5;
}*/


/****************************************************************************
* Function that converts a char to its corresponding alphabetic number.
****************************************************************************/
int charToInt(char ch)
{
   char c = 'a';
   int i;

   i = ch - c;

   return i;
}


/****************************************************************************
* Counts nearby mines in all 8 directions and returns the number of nearby 
* mines found.
****************************************************************************/
int countMines(char minefield[MAX_GRID][MAX_GRID], 
               int row, int col, unsigned size)
{
   int mines = 0;
 
   /* Check up */
   if(row > 0 && minefield[row - 1][col] == MINE)
      mines++;

   /* Check down */
   if(row < size - 1 && minefield[row + 1][col] == MINE)
      mines++;

   /* Check left */
   if(col > 0 && minefield[row][col - 1] == MINE)
      mines++;

   /* Check right */
   if(col < size - 1 && minefield[row][col + 1] == MINE)
      mines++;
 
   /* Check up left */
   if(row > 0 && col > 0 && minefield[row - 1][col - 1] == MINE)
      mines++;

   /* Check up right */
   if(row > 0 && col < size - 1 && minefield[row - 1][col + 1] == MINE)
      mines++;

   /* Check down left */
   if(row < size - 1 && col > 0 && minefield[row + 1][col - 1] == MINE)
      mines++;

   /* Check down right */
   if(row < size - 1 && col < size - 1 && minefield[row + 1][col + 1] == MINE)
      mines++;
 
   return mines;
}


/****************************************************************************
* floodFill is recursive function (algorithm) for 2D array normally used "in 
* the "bucket" fill tool of paint programs to fill connected, 
* similarly-colored areas with a different color, and in games such as Go and 
* Minesweeper for determining which pieces are cleared"
* Wikipedia - accessed and implemented for educational purposes July, 2015.
*
* Also checks for mines in all eight directions until it finds a 
* position with a mine touching that position. It then updates the 
* displayGrid with the number of mines touching that position.
****************************************************************************/
void floodFill(unsigned size, int row, int col, 
               char minefield[MAX_GRID][MAX_GRID], 
               char displayGrid[MAX_GRID][MAX_GRID]) 
{
   int mineCnt;

   if(row < 0 || row > (size - 1)) 
      return;

   if(col < 0 || col > (size - 1)) 
      return;

   if(minefield[row][col] == BLANK && displayGrid[row][col] == UNKNOWN)
   {
      displayGrid[row][col] = BLANK;

      /* Count mines to the left and assign to displayGrid */
      if(col > 0)
      {
         mineCnt = countMines(minefield, row, col - 1, size);

         if(mineCnt > 0)
         {
            displayGrid[row][col - 1] = '0' + mineCnt;
         }
      }

      /* Count mines to the right and assign to displayGrid */
      if(col < size - 1)
      {
         mineCnt = countMines(minefield, row, col + 1, size);

         if(mineCnt > 0)
         {
            displayGrid[row][col + 1] = '0' + mineCnt;
         }
      }

      /* Count mines above and assign to displayGrid */
      if(row > 0)
      {
         mineCnt = countMines(minefield, row - 1, col, size);

         if(mineCnt > 0)
         {
            displayGrid[row - 1][col] = '0' + mineCnt;
         }
      }

      /* Count mines below and assign to displayGrid */
      if(row < size -1)
      {
         mineCnt = countMines(minefield, row + 1, col, size);

         if(mineCnt > 0)
         {
            displayGrid[row + 1][col] = '0' + mineCnt;
         }
      }

      /* Count mines up, left and assign to displayGrid */
      if(row > 0 && col > 0)
      {
         mineCnt = countMines(minefield, row - 1, col - 1, size);

         if(mineCnt > 0)
         {
            displayGrid[row - 1][col - 1] = '0' + mineCnt;
         }
      }

      /* Count mines up, right and assign to displayGrid */
      if(row > 0 && col < size - 1)
      {
         mineCnt = countMines(minefield, row - 1, col + 1, size);

         if(mineCnt > 0)
         {
            displayGrid[row - 1][col + 1] = '0' + mineCnt;
         }
      }

      /* Count mines down, left and assign to displayGrid */
      if(row < size - 1 && col > 0)
      {
         mineCnt = countMines(minefield, row + 1, col - 1, size);

         if(mineCnt > 0)
         {
            displayGrid[row + 1][col - 1] = '0' + mineCnt;
         }
      }

      /* Count mines down, right and assign to displayGrid */
      if(row - size - 1 && col < size - 1)
      {
         mineCnt = countMines(minefield, row + 1, col + 1, size);

         if(mineCnt > 0)
         {
            displayGrid[row + 1][col + 1] = '0' + mineCnt;
         }
      }

      floodFill(size, row + 1, col, minefield, displayGrid);
      floodFill(size, row - 1, col, minefield, displayGrid);
      floodFill(size, row, col + 1, minefield, displayGrid);
      floodFill(size, row, col - 1, minefield, displayGrid);
   } 
}


/****************************************************************************
* processUncoverMine() processes weather a user attempts to uncover a mine
* at any stage of the game and if so, it's automaically game over. The 
* function then updates the display grid with the mines in their respective 
* locations.
****************************************************************************/
void processUncoverMine(char minefield[MAX_GRID][MAX_GRID], 
                        char displayGrid[MAX_GRID][MAX_GRID], 
                        unsigned size, char type, int row, int col)
{
   int i, j;

   for(i = 0; i < size; i++)
   {
      for(j = 0; j < size; j++)
      {
         if(minefield[i][j] == MINE)
         {
            displayGrid[i][j] = MINE;
         }
      }
   }

   printf(" You have uncovered a mine, game over.....\n");
}


/****************************************************************************
* processUncoverNum() checks if user uncovers a numbered square and updates
* the display grid with the uncovered number. 
****************************************************************************/
void processUncoverNum(char minefield[MAX_GRID][MAX_GRID], 
                       char displayGrid[MAX_GRID][MAX_GRID], 
                       char type, int row, int col)
{
   char digit;

   digit = minefield[row][col];
   displayGrid[row][col] = digit;
}


/****************************************************************************
* flagSquare() flags grid references that the user has deduced from the 
* uncovered grid references.
****************************************************************************/
void flagSquare(char minefield[MAX_GRID][MAX_GRID], 
                char displayGrid[MAX_GRID][MAX_GRID], 
                int row, int col)
{
   /* Tally up the amount of flagged mines */ 
   if(displayGrid[row][col] == UNKNOWN)
   {
      displayGrid[row][col] = FLAG;

      if(minefield[row][col] == MINE)
      {
         flagCnt++;
      }
   }

   /* Check if the user tries to flag a numbered square */
   if(isdigit(displayGrid[row][col]))
   {
      printf(" You can't flag a numbered square, try again.\n");
   }

   /* Check if the user tries to flag a blank square */
   if(displayGrid[row][col] == BLANK)
   {
      printf(" You can't flag a blank square, try again.\n");
   }
}


/****************************************************************************
* sweepSquares() invokes the floodFill mechanism on all 8 surrounding 
* squares but only if they are marked as UNKNOWN.
****************************************************************************/
void sweepSquares(char minefield[MAX_GRID][MAX_GRID], 
                  char displayGrid[MAX_GRID][MAX_GRID],
                  int row, int col, unsigned size)
{
   /* If square above is UNKNOWN then call floodFill() */
   if(displayGrid[row - 1][col] == UNKNOWN)
   {
      floodFill(size, row - 1, col, minefield, displayGrid);
   }

   /* If square below is UNKNOWN then call floodFill() */
   if(displayGrid[row + 1][col] == UNKNOWN)
   {
      floodFill(size, row + 1, col, minefield, displayGrid);
   }

   /* If square left is UNKNOWN then call floodFill() */
   if(displayGrid[row][col - 1] == UNKNOWN)
   {
      floodFill(size, row, col - 1, minefield, displayGrid);
   }

   /* If square right is UNKNOWN then call floodFill() */
   if(displayGrid[row][col + 1] == UNKNOWN)
   {
      floodFill(size, row, col + 1, minefield, displayGrid);
   }

   /* If square above left is UNKNOWN then call floodFill() */
   if(displayGrid[row - 1][col - 1] == UNKNOWN)
   {
      floodFill(size, row - 1, col - 1, minefield, displayGrid);
   }

   /* If square above right is UNKNOWN then call floodFill() */
   if(displayGrid[row - 1][col + 1] == UNKNOWN)
   {
      floodFill(size, row - 1, col + 1, minefield, displayGrid);
   }

   /* If square below left is UNKNOWN then call floodFill() */
   if(displayGrid[row + 1][col - 1] == UNKNOWN)
   {
      floodFill(size, row + 1, col - 1, minefield, displayGrid);
   }

   /* If square below right is UNKNOWN then call floodFill() */
   if(displayGrid[row + 1][col + 1] == UNKNOWN)
   {
      floodFill(size, row + 1, col + 1, minefield, displayGrid);
   }
}




